---
title: Three Body Problem
layout: post
date: 2017-09-08
categories: readlist
---

*Disclaimer: It's been a little while since I actually read this book, so my
review might be missing some details or nuance of a fresh read.  On the other
hand, it will have aspects that a fresh review won't have: the color receding
into the distance, the more general shape of the book, how it's faded into the
background of my thoughts.*

Cixin Liu's *Three Body Problem* has been described as China's best science
fiction novel, and been compared to *Dune*, so I knew I'd want to read it for
a while before I actually got around to doing so.  It mostly lives up to the
hype: it's got that great sci-fi combination of the technical, the political,
and the human, and the story unfolds by gradually revealing details in two (or
maybe three) different time periods.  I personally enjoyed reading it for the
illuminating description of the shit of the Revolution in China and for the
realistic exploration of how humanity would react to contact from another
world.

I usually don't care about giving spoilers, but with *Three Body Problem*, I
want to tread lightly, so I'm going to skip the synopsis and go for the three
sci-fi components that work really well.  The technical aspect of this book
centers mostly around the virtual world of the video game *Three Body*, which
features a world with interleaving Chaotic Periods and Stable Periods.  During
a Chaotic Period, the conditions are (duh) chaotic, varying wildly from
ice-cold nights to scorching days of completely random lengths of time.  A
Stable Period is the opposite: day and night fall into rhythms, and life can
flourish until the next Chaotic Period.  We're introduced to the game because
the main character begins playing it in connection with an investigation, and
he begins trying to figure out how the world works.  Through him figuring it
out, Liu explains to the reader the concept of the Three Body Problem of
classical physics, as well as posits a virtual reality suit that players use
to fully immerse themselves into the game.  Although I at first thought the
segments dedicated to *Three Body* were distractions to the main plot, they
eventually reveal themselves as integral to the development of the story, and
by the end of the book were my favorite segments.

Any science fiction worth its salt will use the genre to comment on its day's
political situation: that's what the genre is for.  Liu's book does the same
for the twenty-first century Chinese political situation, which was
interesting on its face because I don't know much about it at all.  The book
opens with a description of a brutal beating and killing of an intellectual
during the Chinese Revolution, and that brutality, the extreme dedication to
an idea disregarding its cost in human life, persists throughout the book.
The interpersonal dynamics in *Three Body Problem* feel real, as do the
international politics that are represented in those dynamics, especially
during the summit meetings held later in the novel.  Liu spends some time
discussing theories as to how we would react to extraterrestrial communication
(spoiler alert: it's almost certainly going to be terrible), and through the
novel explores how those theories might really play out.

The human aspect of the story was the hardest for me to get into.  I'm not
sure if it's something to do with the cultural difference between myself and
the author or something else, but it was challenging to be rooting for a
character one minute to have them betray all of humanity the next without much
explanation as to what led them down that path.

The book has a few loose ends as well: for example, a mysterious countdown
that does not get resolved.  However, I found out that *Three Body Problem* is
only the first of a trilogy by Cixin Liu, so I'm sure the ends will be tied
together by the end of book III (I definitely reserved the next in the series
from the library as soon as I finished the first!).

Overall, I'd recommend *Three Body Problem* as a new science fiction novel
that explores some really interesting territory and sheds light on a country I
personally knew little about.
