---
layout: page
title: Colophon
permalink: /colophon/
---

This site is published on Gitlab Pages using Jekyll.  I usually edit it with the
Neovim text editor, but sometimes (like now, at work) I use the embedded text
editor with Gitlab.  The font that I use (and that eventually I will include
somehow with the site) is Computer Modern Typewriter Text Variable Width, so if
you really want to see how it *can* look, install that font.

This site and all writings contained within are licensed under the CC-BY-SA
license (see LICENSE at the [gitlab repo][]).  Here's an icon that provides more
information:

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
<img alt="Creative Commons License" style="border-width:0" 
    src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>
<br />This <span xmlns:dct="http://purl.org/dc/terms/" 
    href="http://purl.org/dc/dcmitype/Text" rel="dct:type">work</span>
by <a xmlns:cc="http://creativecommons.org/ns#" 
    href="duckwork.gitlab.io" property="cc:attributionName" 
    rel="cc:attributionURL">Case Duckworth</a> 
is licensed under a <a rel="license" 
    href="http://creativecommons.org/licenses/by-sa/4.0/">
Creative Commons Attribution-ShareAlike 4.0 International License</a>.

[gitlab repo]: https://gitlab.com/acdw/acdw.gitlab.io